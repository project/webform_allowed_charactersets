<?php

namespace Drupal\webform_allowed_charactersets\Service;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Characterset service.
 */
class CharactersetService {

  /**
   * The forms input fields.
   *
   * @var array
   */
  protected $inputFields = [];

  /**
   * Add validation to input fields.
   *
   * Recursively loops a given form or element.
   * Adds element validation handler to each input field.
   *
   * @param array $element
   *   The Form API form or a child element to check if it is or has an input field.
   */
  public function addCharactersetValidation(&$element) {
    // If its an input field.
    if (is_array($element) && isset($element['#type']) && in_array($element['#type'], ['textfield', 'textarea'])) {
      // Add extra validation.
      $element['#element_validate'][] = [$this, 'validateCharacterset'];
      // Add it to the global array of input fields.
      $this->inputFields[] = $element;

    }
    // Recursive loop nested children.
    foreach (Element::children($element) as $key) {
      $child = &$element[$key];
      $this->addCharactersetValidation($child);
    }
  }

  /**
   * Validates form state for valid charactersets.
   *
   * Recursively loops form state.
   * BlaBla
   *
   * @param $element
   *   The Form API form.
   */
  public function validateFormState($element, FormStateInterface $form_state) {
    // If its an input field.
    if (isset($element['#webform_key']) && is_array($element) && isset($element['#type']) && in_array($element['#type'], ['textfield', 'textarea'])) {
      // Get input value.
      $key = $element['#webform_key'];
      $input = $form_state->getValue($key);
      // Validate.
      if (!$this->isValidCharacterset($input)) {
        // Set error if input is not a valid characterset.
        $form_state->setErrorByName($key,t('Invalid input'));
      }
    }
    // Recursive loop nested children.
    foreach (Element::children($element) as $key) {
      $child = &$element[$key];
      $this->validateFormState($child, $form_state);
    }
  }

  /**
   * #element_validate handler for webform input fields.
   *
   * Checks if the given input is valid according with the allowed characterset settings.
   */
  public function validateCharacterset(array $element, FormStateInterface $form_state) {
    if (!isset($element['#webform_key'])) {
      return;
    }
    // Get the input value.
    $key = $element['#webform_key'];
    $input_text = $form_state->getValue($key);
    // Check if its valid.
    if (!$this->isValidCharacterset($input_text)) {
      $form_state->setError($element, t('Invalid input'));
    }

  }

  private function isValidCharacterset($text) {

    // Character set regular expressions.
    $characterset_regexes = [
      "latin" => '\p{Latin}',
      "cyrillic" => '\p{Cyrillic}',
      "chinese" => '\x{4e00}-\x{9fff}',
      "japanese" => '\p{Hiragana}\p{Katakana}\p{Han}',
      "korean" => '\x{1100}-\x{11FF}\x{3130}-\x{318F}\x{AC00}-\x{D7AF}',
      "arabic" => '\p{Arabic}',
      "greek" => '\p{Greek}',
      "hebrew" => '\p{Hebrew}',
      "devanagari" => '\p{Devanagari}',
      "thai" => '\p{Thai}',
      "tamil" => '\p{Tamil}',
      "bengali" => '\p{Bengali}',
      "punjabi" => '\p{Gurmukhi}',
      "gujarati" => '\p{Gujarati}',
      "kannada" => '\p{Kannada}',
      "telugu" => '\p{Telugu}',
      "malayalam" => '\p{Malayalam}',
      "georgian" => '\p{Georgian}',
      "armenian" => '\p{Armenian}',
      "tibetan" => '\p{Tibetan}'
      // Add more character sets here
    ];

    // Get and loop the valid charactersets.
    $config = \Drupal::config('webform_allowed_charactersets.settings');
    $charactersets = $config->get('charactersets');
    foreach ($charactersets as $characterset => $enabled) {
      if (!$enabled) {
        continue;
      }
      // Get the regex for checking that characterset.
      $regex = $characterset_regexes[$characterset];

      // Temp fix.
      if (!is_string($text)) {
        return TRUE;
      }
      // If the text matches a valid characterset, return TRUE.
      if (preg_match('/[' . $regex . ']/u', $text)) {
        return TRUE;
      }
    }

    // If we are here, given text does not match any valid characterset.
    return FALSE;
  }

}

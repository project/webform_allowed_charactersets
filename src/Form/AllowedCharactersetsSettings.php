<?php

namespace Drupal\webform_allowed_charactersets\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settingsform for this module.
 */
class AllowedCharactersetsSettings extends ConfigFormBase {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a settings controller.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler) {
    parent::__construct($config_factory);
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['webform_allowed_charactersets.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wac_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Validate existence of the webform module. Should not be needed because of the requirements, but just to be sure.
    if (!$this->moduleHandler->moduleExists('webform')) {
      return $form;
    }

    // Form constructor.
    $form = parent::buildForm($form, $form_state);

    // Get this module config.
    $config = $this->config('webform_allowed_charactersets.settings');

    // Enable / disable webform characterset validation.
    $form['enable_characterset_validation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable characterset validation'),
      '#default_value' => $config->get('enable_characterset_validation') ?? FALSE,
      '#description' => $this->t('Enables webform characterset validation. If input is not a valid characterset, form will fail to validate and can not be submitted.'),
    ];

    // Protect all forms ?
    $form['protect_all_forms'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use it on all webforms'),
      '#default_value' => $config->get('protect_all_forms') ?? FALSE,
      '#description' => $this->t('If you enable this, then characterset validation is applied to all webforms. If not, you need to add the webform handler "Validate input characerset" on individual forms.'),
    ];

    // List of possible options: 20 most common charactersets. To be expanded in the future ?
    $options = [
      'latin' => 'Latin (English, French, Spanish, etc.)',
      'cyrillic' => 'Cyrillic (Russian, Ukrainian, Bulgarian, etc.)',
      'chinese' => 'Chinese (Simplified and Traditional)',
      'japanese' => 'Japanese (Hiragana, Katakana, Kanji)',
      'korean' => 'Korean (Hangul)',
      'arabic' => 'Arabic',
      'greek' => 'Greek',
      'hebrew' => 'Hebrew',
      'devanagari' => 'Devanagari (Hindi, Nepali, Marathi, etc.)',
      'thai' => 'Thai',
      'tamil' => 'Tamil',
      'bengali' => 'Bengali',
      'punjabi' => 'Punjabi',
      'gujarati' => 'Gujarati',
      'kannada' => 'Kannada',
      'telugu' => 'Telugu',
      'malayalam' => 'Malayalam',
      'georgian' => 'Georgian',
      'armenian' => 'Armenian',
      'tibetan' => 'Tibetan'
    ];

    // Display possible charactersets as chackboxes.
    $form['charactersets'] = [
      '#type' => 'checkboxes',
      '#title' => 'Allowed charactersets',
      '#options' => $options,
      '#default_value' => $config->get('charactersets') ?? [],
      '#description' => $this->t('Select which charactersets that are allowed. If nothing is checked, then everything is allowed and there will be no characterset validation on webform submissions.'),
      '#states' => [
        'disabled' => [
          ':input[name="enable_characterset_validation"]' => ['checked' => FALSE],
        ],
        'required' => [
          ':input[name="enable_characterset_validation"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // TODO: Negate the characterset condition.
    // $form['charactersets_negate'] = [
    //   '#type' => 'checkbox',
    //   '#title' => $this->t('Negate the condition'),
    //   '#default_value' => $config->get('charactersets_negate') ?? FALSE,
    //   '#description' => $this->t('If checked, then webform submissions containing any of the checked charactersets will be blocked.'),
    // ];

    $form['actions']['#type'] = 'container';
    $form['actions']['submit']['#value'] = $this->t('Save configuration');
    $form['actions']['#weight'] = 4;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('webform_allowed_charactersets.settings');
    $config->set('enable_characterset_validation', $form_state->getValue('enable_characterset_validation'));
    $config->set('protect_all_forms', $form_state->getValue('protect_all_forms'));
    $config->set('charactersets', $form_state->getValue('charactersets'));
    // $config->set('charactersets_negate', $form_state->getValue('charactersets_negate'));

    // Save config settings.
    $config->save();

    return parent::submitForm($form, $form_state);
  }

}

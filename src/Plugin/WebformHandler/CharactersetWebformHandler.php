<?php

namespace Drupal\webform_allowed_charactersets\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

/**
 * Webform validate handler.
 *
 * @WebformHandler(
 *   id = "allowed_charactersets_handler",
 *   label = @Translation("Validate input characerset"),
 *   category = @Translation("Settings"),
 *   description = @Translation("This will validate input based on the allowed charactersets from /admin/structure/webform/config/charactersets."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class CharactersetWebformHandler extends WebformHandlerBase {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    // Get this modules config.
    $config = \Drupal::config('webform_allowed_charactersets.settings');
    // Check if characterset validation should be added.
    if ($config->get('enable_characterset_validation')) {
        // Add custom form validation for charactersets.
        \Drupal::service('webform_allowed_charactersets.charactersetservice')->validateFormInput($form, $form_state);
    }
  }

  /**
   * Validate if entered email addresses are not existing users.
   *
   * Webform already checks if email is not empty (its required) and if its valid.
   * We need to check if there is already a user with that emailaddress.
   */
  private function validateEmails(FormStateInterface $formState) {
    // Check the main email address of the form.
    $email = !empty($formState->getValue('e_mailadres')) ? Html::escape($formState->getValue('e_mailadres')) : FALSE;
    $username = !empty($formState->getValue('name')) ? Html::escape($formState->getValue('name')) : FALSE;
    if ($email && $this->checkIfUserEmailExists($email)) {
        $formState->setErrorByName(
            'e_mailadres',
            $this->t('@email is already registered.', ['@email' => $email])
        );
    }
    if ($username && $this->checkIfUserNameExists($username)) {
      $formState->setErrorByName(
          'name',
          $this->t('@email is already registered.', ['@email' => $username])
      );
  }

    // If its a cluster, check all other email addresses.
    if ($formState->getValue('hoe_wil_je_inschrijven_') == 'cluster') {
      $users = $formState->getValue('extra_users');
      $i = 0;
      foreach ($users as $user) {
        if ($this->checkIfUserEmailExists($user['email'])) {
          $key = 'extra_users][items][' . $i . '][email';
          $formState->setErrorByName(
            $key,
            $this->t('@email is already registered.', ['@email' => $user['email']])
          );
        }
        if ($this->checkIfUserNameExists($user['name'])) {
          $key = 'extra_users][items][' . $i . '][name';
          $formState->setErrorByName(
            $key,
            $this->t('@email is already registered.', ['@email' => $user['name']])
          );
        }
        $i++;
      }
    }
  }

}
